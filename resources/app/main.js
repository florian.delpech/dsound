const electron = require('electron');
const {ipcMain} = require('electron');//Communication electron / html
const Dexie = require('dexie');//Gestion base de données locale
const async = require('async');//Permet de faire des foreach et autres
const fs = require('fs'); 
var request = require('request');//Telechargement de fichiers

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const env = 'prod'; //values : dev, prod

var prodPath = 'resources/app/';
var pathToSongs = (env === 'dev')?'uploads/songs/':prodPath+'uploads/songs/';
var pathToPictures = (env === 'dev')?'uploads/pictures/':prodPath+'uploads/pictures/';
var pathToMessages = (env === 'dev')?'uploads/messages/':prodPath+'uploads/messages/';

var songsToNotDelete = [];
var messagesToNotDelete = [];
var picturesToNotDelete = [];

//console.log(pathToSongs, pathToPictures, pathToMessages);

var serverPath = 'http://d-sound.fr';
var serverPathMessage = 'http://d-sound.fr/messages/';

let mainWindow;


//Creation de la fenetre principale
function createWindow () {
	
    //console.log('Create dsound');
    mainWindow = new BrowserWindow({
            width: 1800,
            height: 1200,
            title: 'D-sound',
            movable: true
    }); // on définit une taille pour notre fenêtre

    mainWindow.loadURL(`file://${__dirname}/index.html`); // on doit charger un chemin absolu
    //mainWindow.openDevTools();
    mainWindow.on('closed', () => {
            mainWindow = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

function showProgress(received,total){
    var percentage = (received * 100) / total;
    //console.log(percentage + "% | " + received + " bytes out of " + total + " bytes.");
}

//Telechargement d un tableau de fichiers
function downloadNextFiles(filesToDownload, event){
    //On regarde si le fichier audio existe deja
    if(typeof filesToDownload[0] != 'undefined'){

        var filename = filesToDownload[0].localfile.replace('/uploads/pictures/','');
        var type = filesToDownload[0].type;
        //console.log('===============');
        //console.log('verification du fichier: '+filename);
        fs.stat(filename, function(err, stat) { 
            console.log('DL', filesToDownload[0]);
            if(typeof filesToDownload[0].path != 'undefined' && filesToDownload[0].path != null && filesToDownload[0].path != 'http://d-sound.frnull'){
                if(err == null) {
                    //console.log('type :'+type);
                    //console.log('File '+filesToDownload[0].path+' exist...');
                    filesToDownload.splice(0, 1);

                    if(type !== 'image') event.sender.send('callback-download-songs', 'file here !');
                    if(filesToDownload.length > 0) downloadNextFiles(filesToDownload, event);                   
                }else{
                    //console.log('DONWLOAD '+filesToDownload[0].path+' IN '+filesToDownload[0].destination);
                    //On telecharge le titre
                    var file_url = filesToDownload[0].path;
                    console.log('On telecharge le fichier : '+file_url);
                    var targetPath = filename;

                    // Save variable to know progress
                    var received_bytes = 0;
                    var total_bytes = 0;

                    var req = request({
                        method: 'GET',
                        uri: file_url
                    });

                    var out = fs.createWriteStream(targetPath);
                    req.pipe(out);

                    req.on('response', function ( data ) {
                        // Change the total bytes value to get progress later.
                        total_bytes = parseInt(data.headers['content-length' ]);
                    });

                    req.on('data', function(chunk) {
                        // Update the received bytes
                        received_bytes += chunk.length;

                        showProgress(received_bytes, total_bytes);
                    });

                    req.on('end', function() {
                        //console.log("File succesfully downloaded");
                        //Renvoyer a la vue que le téléchargement est ok
                        filesToDownload.splice(0, 1);
                        if(filesToDownload.length > 0) downloadNextFiles(filesToDownload, event);
                        //console.log('type :'+type);
                        if(type !== 'image') event.sender.send('callback-download-songs', 'file here !');
                    });		

                }
            }else{
                console.log('Probleme fichier ', filesToDownload[0]);
                filesToDownload.splice(0, 1);
                if(filesToDownload.length > 0) downloadNextFiles(filesToDownload, event);
            }
            			
        });	
    }

}

function deleteOldFiles(filesToNotDelete, path){
    var files = fs.readdirSync(path);
    //console.log('filesToNotDelete', filesToNotDelete);
    files.forEach(function(file) {
        if (!fs.statSync(path + file).isDirectory()) {
            //console.log('file :'+file);
            if(filesToNotDelete.indexOf(file) == -1){
                fs.unlink(path + file, (err) => {
                    if (err) {
                        //alert("An error ocurred updating the file" + err.message);
                        return;
                    }
                    //console.log("File succesfully deleted");
                });
            }
        //console.log('SONGS IN FOLDER: '+path + file);
        }
    });
}

//On reçoi la liste des titres à télécharger de dsound  
ipcMain.on('download-songs', (event, arg) => {
	
    var obj = JSON.parse(arg);
    var filesToDownload = [];
    songsToNotDelete = [];
    messagesToNotDelete = [];
    picturesToNotDelete = [];

    //On parcours toutes les chansons
    async.each(obj, q => {
        if(q.type == 'message'){
            filesToDownload.push({'type':'message', 'path':serverPathMessage+q.filename,'destination':pathToMessages, 'localfile':pathToMessages+q.filename});
            messagesToNotDelete.push(q.filename);
        }else{
            //Ajout de l image
            filesToDownload.push({'type':'image', 'path':serverPath+q.picture,'destination':pathToPictures, 'localfile':pathToPictures+q.picture});
            if(q.picture && q.picture != null){
                picturesToNotDelete.push(q.picture.replace('/uploads/pictures/',''));
            }
            
            //Ajout de la musique
            filesToDownload.push({'type':'track', 'path':serverPath+"/mp3/"+q.filenamemp3,'destination':pathToSongs, 'localfile':pathToSongs+q.filenamemp3});	
            songsToNotDelete.push(q.filenamemp3);
            //@TODO On supprime les titres qui ne sont plus dans les playlists
        }

    });
    //console.log(filesToDownload.length +' Fichiers a télécharger...');
    downloadNextFiles(filesToDownload, event);
    deleteOldFiles(songsToNotDelete, pathToSongs);
    deleteOldFiles(messagesToNotDelete, pathToMessages);
    deleteOldFiles(picturesToNotDelete, pathToPictures);
});

//On verifie que le fichier a jouer exist dans les dossiers  
ipcMain.on('check-file-exist', (event, arg) => {
    
    var url = JSON.parse(arg);
    var filePath = env === 'dev'?url:prodPath+url;
    //console.log('verification du fichier: '+filePath);
        fs.stat(filePath, function(err, stat) { 
            if(err == null) {
                //console.log('Le fichier '+filePath+' exist ! ');
                event.sender.send('callback-check-file-exist', url);                        
            }else{
                //console.log('Le fichier '+filePath+' n exist pas ! ');
                event.sender.send('callback-check-file-exist', false);
            }			
        });
    
});

//On supprime les fichiers que le lecteur ne parvient pas a lire 
ipcMain.on('delete-error-file', (event, arg) => {
    
    var url = JSON.parse(arg);
    var filePath = env === 'dev'?url:prodPath+url;
    //console.log('suppresion du fichier: '+filePath);
    fs.unlink(filePath, (err) => {
        if (err) {
            alert("An error ocurred updating the file" + err.message);
            return;
        }
        //console.log("File succesfully deleted");
    });
});