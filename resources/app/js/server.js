;

(function ($) {

    //serer: Object Instance : Communications avec le serveur electron
    $.server = function(el, options) {
		
        var serv = $(el);
        // making variables public
        serv.vars = $.extend({}, $.server.defaults, options);
		jQuery.data(el, "server", serv);//
		
        var methods = {};
        serv.cptdl = 0;
        // Private serv methods
        methods = {
            init: function() {
				
                //Initialisation de la BDD
                methods.initializeDataBase();

                //Callback du telechargement d un fichier
                ipcRenderer.on('callback-download-songs', (event, arg) => {
                    serv.cptdl++;
                    $('#dl-done').html(serv.cptdl);
                });
                //methods.deleteDataBase();
							
            },
            //Initialisation de la base de donnees
            initializeDataBase: function(){
                serv.db = new Dexie("dsound");
                serv.db.version(1).stores({
                  users: 'id,date_last_synchro,&[username+password]',
                  playlists: 'id,user_id,name,create_at,description,songscount',
                  songs: 'id,user_id,title,playlist_id,artist,filenamemp3,picture,playtime_string',
                  messages: 'id,user_id,name,active_week,active_year,create_at,decibel,filename,hour_send,type_play',
                  days: 'id,message_id,planning_id, name,days_id,day_order',
                  dates: 'id,message_id,planning_id, start_date,end_date,create_at',
                  diffusions: 'id,user_id,name',
                  plages: 'id,diffusion_id,nbSongs',
                  messagesToPlage: 'id,plage_id,name,filename',
                  plannings: 'id,user_id,name,priority,active_year,active_week',
                  daysToPlanning: 'id, day, planning_id, start, end, playlist_id, playlist'
                });
            },
            //Suppression de la base de donnees
            deleteDataBase: function(){
                serv.db.delete();
            },
            //Insertion user local bdd	
            insertUser: function(id, username, password, date_last_synchro){
                var obj = {id: parseInt(id), username: username, password: password};
                if(typeof date_last_synchro !== 'undefined'){
                    obj['date_last_synchro'] = date_last_synchro;
                }
                serv.db.users.put(obj).then(methods.logAddRecord);
            },
            //Insertion user local bdd	
            updateLastSynchroUser: function(id, date_last_synchro){
                console.log('UpdateLastSynchroUSer');
                serv.db.users.update(parseInt(id), {date_last_synchro: date_last_synchro}).then(methods.logAddRecord);
            },
            //Insertion planning local bdd	
            insertPlanning: function(planning){
                var obj = {id: parseInt(planning.id), user_id:parseInt(planning.user_id), name: planning.name, active_week: planning.active_week, active_year: planning.active_year, priority:planning.prioriy};
                serv.db.plannings.put(obj).then(methods.logAddRecord);
            },
            //Insertion jours/playlist a jouer dans la semaine du planning local bdd	
            insertDaysInPlanning: async function(planning){
                serv.db.daysToPlanning.where('planning_id').equals(parseInt(planning.id)).delete().then(function () {
                    jQuery.each(planning.planning, function(key, day){
                        jQuery.each(day, function(key, playlist){
                            var s = {id: parseInt(playlist.id), planning_id:parseInt(planning.id), day:playlist.day, playlist_id: parseInt(playlist.playlist_id), start: playlist.start, end: playlist.end, playlist: playlist.playlist};
                            serv.db.daysToPlanning.put(s).then(methods.logAddRecord);
                        });
                    });
                });                
            },
            //Insertion jours de diffusion du planning local bdd	
            insertDaysToPlanning: async function(planning){                 
                serv.db.days.where('planning_id').equals(parseInt(planning.id)).delete().then(function () {
                    jQuery.each(planning.days, function(key, day){
                        var s = {id: parseInt(''+planning.id+day.days_id), planning_id:parseInt(planning.id), name:day.name, days_id: parseInt(day.days_id), day_order: parseInt(day.day_order)};
                        serv.db.days.put(s).then(methods.logAddRecord);
                    });
                });

            },
            //Insertion dates planning local bdd	
            insertDatesToPlanning: async function(planning){
                serv.db.dates.where('planning_id').equals(parseInt(planning.id)).delete().then(function () {
                    jQuery.each(planning.dates, function(key, date){
                        var s = {id: parseInt(date.id), planning_id:null, planning_id:parseInt(date.plannings_id), start_date:date.start_date, end_date:date.end_date, create_at: date.create_at};
                        serv.db.dates.put(s).then(methods.logAddRecord);
                    });
                });               
            },
            //Insertion diffusion local bdd	
            insertDiffusion: function(diffusion){
                var obj = {id: parseInt(diffusion.id), user_id: parseInt(diffusion.user_id),name: diffusion.name};
                serv.db.diffusions.put(obj).then(methods.logAddRecord);
                jQuery.each(diffusion.plages, function(key, plage){
                    methods.insertPlage(plage);
                });
                
            },
             //Insertion plage local bdd	
            insertPlage: function(plage){
                var obj = {id: parseInt(plage.id), diffusion_id:parseInt(plage.diffusion_id), nbSongs: plage.nbSongs};
                serv.db.plages.put(obj).then(methods.logAddRecord);
                jQuery.each(plage.messages, function(key, mp){
                    methods.insertMessagesToPlage(mp, key);
                });
            },
             //Insertion messagetoplage local bdd	
            insertMessagesToPlage: function(mp, index){
                               
                serv.db.messagesToPlage.where('plage_id').equals(parseInt(mp.diffusion_plages_id)).delete().then(function () {
                    var obj = {id: parseInt(''+index+mp.id+mp.diffusion_plages_id), message_id: parseInt(mp.id), plage_id:parseInt(mp.diffusion_plages_id), name: mp.name, filename: mp.filename};
                    serv.db.messagesToPlage.put(obj).then(methods.logAddRecord);
                });
                
            },
            //Insertion message local bdd	
            insertMessage: function(message){
                console.log('message.user_id', message.user_id);
                var obj = {id: parseInt(message.id), user_id:parseInt(message.user_id), type:'message', name: message.name, create_at: message.create_at, active_week: message.active_week, active_year: message.active_year, decibel: message.decibel, filename: message.filename, hour_send: message.hour_send, type_play: message.type_play};
                serv.db.messages.put(obj).then(methods.logAddRecord);
            },
            
            //Insertion jours message local bdd	
            insertDaysToMessage: async function(message){                 
                serv.db.days.where('message_id').equals(parseInt(message.id)).delete().then(function () {
                    jQuery.each(message.days, function(key, day){
                        var s = {id: parseInt(''+message.id+day.days_id), message_id:parseInt(message.id), name:day.name, days_id: parseInt(day.days_id), day_order: parseInt(day.day_order)};
                        serv.db.days.put(s).then(methods.logAddRecord);
                    });
                });

            },
            //Insertion dates local bdd	
            insertDatesToMessage: async function(message){
                serv.db.dates.where('message_id').equals(parseInt(message.id)).delete().then(function () {
                    jQuery.each(message.dates, function(key, date){
                        var s = {id: parseInt(date.id), message_id:parseInt(message.id), plannings_id:parseInt(date.plannings_id), start_date:date.start_date, end_date:date.end_date, create_at: date.create_at};
                        serv.db.dates.put(s).then(methods.logAddRecord);
                    });
                });               
            },
            //Insertion playlists local bdd	
            insertPlaylist: function(playlist){

                var obj = {id: parseInt(playlist.id), user_id:parseInt(playlist.user_id), name: playlist.name, create_at: playlist.create_at, description: playlist.description, songscount: playlist.songscount};
                serv.db.playlists.put(obj).then(methods.logAddRecord);

            },
            //Insertion songs local bdd
            insertSongs: function(playlist){
                jQuery.each(playlist.songs, function(key, song){
                    
                    var s = {
                        id: parseInt(playlist.id+song.id), 
                        user_id: parseInt(playlist.user_id),
                        title:song.title, playtime:song.playtime_string, 
                        playlist_id: parseInt(playlist.id), 
                        artist: song.artist, 
                        filenamemp3: song.filenamemp3, 
                        picture: song.picture, 
                        picturelocal: (typeof song.picture != 'undefined' && song.picture != null)?song.picture.substr(1):'', 
                        playtime_string: song.playtime_string};
                    serv.db.songs.put(s).then(methods.logAddRecord);
                });
            },
            getMessagesByPlage: async function(plage, messages){      
                return await serv.db.messagesToPlage.where('plage_id').equals(plage.id).toArray().then(function (ms) {
                    jQuery.each(ms, function(key, m){                   
                        var mi = messages.find(obj => {
                            return obj.id === m.message_id;
                        });  
                        m.dates = mi.dates;
                        m.jours = mi.jours;
                        m.type = mi.type;
                        m.active_week = mi.active_week;
                        m.active_year = mi.active_year;
                    });
                    
                    return ms;		
                });
            },
            getPlagesByDiffusion: async function(plage){
                return await serv.db.plages.where('diffusion_id').equals(plage.diffusion_id).toArray().then(function (plages) {
                        return plages;		
                });
            },
            getSongsByPlaylist: async function(playlist){

                return await serv.db.songs.where('playlist_id').equals(playlist.id).toArray().then(function (songs) {
                        return songs;		
                });

            },      
            getDatesByMessage: async function(message){
                return await serv.db.dates.where('message_id').equals(message.id).toArray().then(function (dates) {
                    return dates;		
                });
            },
            getJoursByMessage: async function(message){
                return await serv.db.days.where('message_id').equals(message.id).toArray().then(function (days) {
                    return days;		
                });
            },
            getJoursByPlanning: async function(planning){
                return await serv.db.days.where('planning_id').equals(planning.id).toArray().then(function (days) {
                    return days;		
                });
            },
            getDatesByPlanning: async function(planning){
                return await serv.db.dates.where('planning_id').equals(planning.id).toArray().then(function (dates) {
                    return dates;		
                });
            },
            getJoursInPlanningByPlanning: async function(planning){
                return await serv.db.daysToPlanning.where('planning_id').equals(planning.id).toArray().then(function (days) {
                    return days;		
                });
            },
            logAddRecord: function(){
                //console.log("Record added!");
            },
            logGetLocalData: function(data){
                //console.log("getData", data);
            }

        };

        // public methods
        serv.deleteDataBase = function(){
            methods.deleteDataBase();  
            methods.initializeDataBase();
        };
        // // //join a une colelction d objet playlist les objets associés (ici les chanssons)
        serv.joinMessagesToPlages = function(plagesCollection) {
            var all = Dexie.Promise.all;
            // Start by getting all bands as an array of band objects
            return plagesCollection.toArray(function(plages) {

                // Query related properties:
                var messagesPromises = plages.map(function (plage) {
                    var messages = serv.getMessages();
                    return messages.then(function(ms){
                        return methods.getMessagesByPlage(plage, ms);
                    });

                });
                // Await genres and albums queries:
                return all ([
                        all(messagesPromises) //, all(albumsPromises)
                ]).then(function (messages) { //songsAndAlbums
                    // Now we have all foreign keys resolved and
                    // we can put the results onto the bands array
                    // before returning it:
                    plages.forEach(function (plage, i) {
                            plage.messages = messages[0][i];
                    });
                    return plages;
                });
            });
        };
        // //join a une colelction d objet playlist les objets associés (ici les chanssons)
        serv.joinPlages = function(diffusionCollection) {
            var all = Dexie.Promise.all;
            // Start by getting all bands as an array of band objects
            return diffusionCollection.toArray(function(diffusions) {

                // Query related properties:
                var plagesPromises = diffusions.map(function (diffusion) {
                    return serv.joinMessagesToPlages(serv.db.plages.where('diffusion_id').equals(diffusion.id)).then(function (plages) {
                        return plages;    
                    }).catch(function (error) {
                        console.log("Oops: " + error);
                    });
                });
                // Await genres and albums queries:
                return all ([
                        all(plagesPromises) //, all(albumsPromises)
                ]).then(function (plages) { //songsAndAlbums
                    // Now we have all foreign keys resolved and
                    // we can put the results onto the bands array
                    // before returning it:
                    diffusions.forEach(function (diffusion, i) {
                            diffusion.plages = plages[0][i];
                    });
                    return diffusions;
                });
            });
        };
        //join a une colelction d objet playlist les objets associés (ici les chanssons)
        serv.joinPlaylists = function(playlistCollection) {
            var all = Dexie.Promise.all;
            // Start by getting all bands as an array of band objects
            return playlistCollection.toArray(function(playlists) {

                // Query related properties:
                var songsPromises = playlists.map(function (playlist) {
                        return methods.getSongsByPlaylist(playlist);
                });
                // Await genres and albums queries:
                return all ([
                        all(songsPromises) //, all(albumsPromises)
                ]).then(function (songs) { //songsAndAlbums
                    // Now we have all foreign keys resolved and
                    // we can put the results onto the bands array
                    // before returning it:
                    playlists.forEach(function (playlist, i) {
                            playlist.songs = songs[0][i];
                    });
                    return playlists;
                });
            });
        };
        //join a une collection objet message les objets associés (ici les dates et jours)
        serv.joinMessages = function(messageCollection) {
            var all = Dexie.Promise.all;
            // Start by getting all bands as an array of band objects
            return messageCollection.toArray(function(messages) {

                // Query related properties:
                var datesPromises = messages.map(function (message) {
                        return methods.getDatesByMessage(message);
                });
                var joursPromises = messages.map(function (message) {
                        return methods.getJoursByMessage(message);
                });
                // Await genres and albums queries:
                return all ([
                    all(datesPromises), //, all(albumsPromises)
                    all(joursPromises)
                ]).then(function (datesAndJours) { //songsAndAlbums
                    // Now we have all foreign keys resolved and
                    // we can put the results onto the bands array
                    // before returning it:
                    messages.forEach(function (message, i) {
                        message.dates = datesAndJours[0][i];
                        message.jours = datesAndJours[1][i];
                    });
                    return messages;
                });
            });
        };
        //join a une collection objet message les objets associés (ici les dates et jours)
        serv.joinPlannings = function(planningCollection) {
            var all = Dexie.Promise.all;
            // Start by getting all bands as an array of band objects
            return planningCollection.toArray(function(plannings) {

                // Query related properties:
                var datesPromises = plannings.map(function (planning) {
                        return methods.getDatesByPlanning(planning);
                });
                var joursPromises = plannings.map(function (planning) {
                        return methods.getJoursByPlanning(planning);
                });
                var joursPlanningPromises = plannings.map(function (planning) {
                        return methods.getJoursInPlanningByPlanning(planning);
                });
                // Await genres and albums queries:
                return all ([
                    all(datesPromises), //, all(albumsPromises)
                    all(joursPromises),
                    all(joursPlanningPromises)
                ]).then(function (datesAndJoursAndPlanning) { //songsAndAlbums
                    // Now we have all foreign keys resolved and
                    // we can put the results onto the bands array
                    // before returning it:
                    plannings.forEach(function (planning, i) {
                        var p = {'lundi':[], 'mardi':[], 'mercredi':[], 'jeudi':[], 'vendredi':[], 'samedi':[], 'dimanche':[]};
                        datesAndJoursAndPlanning[2][i].forEach(function (day, i) {
                           p[day.day].push(day); 
                        });
                        planning.dates = datesAndJoursAndPlanning[0][i];
                        planning.jours = datesAndJoursAndPlanning[1][i];
                        planning.plannings = p;
                    });
                    return plannings;
                });
            });
        };
        
        //Permet de debugger la BDD
        serv.getLocalData = async function() {	
		
            /*var result = await serv.db.playlists.toArray();
            methods.logGetLocalData(result);

            var tracks = await serv.db.songs.toArray();
            methods.logGetLocalData(tracks);
            
            var messages = await serv.db.messages.toArray();
            var diffusion = await serv.db.diffusions.toArray();
            
            var plages = await serv.db.plages.toArray();
            var messagesToPlage = await serv.db.messagesToPlage.toArray();*/
            
            /*var plannings = await serv.db.plannings.toArray();
            var messages = await serv.db.messages.toArray();
            var diffusion = await serv.db.diffusions.toArray();

            console.log('LOCAL DATA', 'plannings', plannings, 'messages', messages, 'diffusion', diffusion);*/
            
        };
        
        serv.downloadTracks = async function(userId) {
            var messages = await serv.db.messages.where('user_id').equals(parseInt(userId)).toArray();
            var tracks = await serv.db.songs.where('user_id').equals(parseInt(userId)).toArray();
            
            var files = tracks.concat(messages);
            console.log('DOWNLOAD FILE ', files);
            $('#download-cpt').html('<span id="dl-done">0</span> / '+files.length);
            ipcRenderer.send('download-songs', JSON.stringify(files));
        };
        //recuperation des donnees sur d-sound
        serv.getDistantData = function(userId) {
            
            var users = serv.checkUser($('#user_name').val(), $('#user_password').val());//verification de l'utilisateur
                    
            return users.then(function(users){
                if(typeof users[0].id !== 'undefined'){
                    $.ajax({
                        method: "POST",
                        url: "http://d-sound.fr/webservice/v1/connexion",
                        data: { 
                            "user_name": $('#user_name').val(), 
                            "user_password": $('#user_password').val() 
                        }
                    }).done(function( msg ) {           
                        var obj = jQuery.parseJSON( msg );
                        console.log('getDistantData -> recuperation du user', obj);
                        if(typeof obj.user_id !== 'undefined' && obj.user_id){
                            var localDate = new Date(users[0].date_last_synchro);
                            var localDateTimestamp = localDate.getTime();
                            var distantDate = new Date(obj.last_date_synchro);
                            var distantDateTimestamp = distantDate.getTime();
                            //Si une mise a jour a ete faite cote server, on recupere les donnees
                            console.log('getDistantData -> comparaison date synchro '+distantDateTimestamp+' > '+localDateTimestamp);
                            if(distantDateTimestamp == 'NaN' || localDateTimestamp == 'NaN' || distantDateTimestamp > localDateTimestamp){
                                methods.updateLastSynchroUser(obj.user_id, obj.last_date_synchro);
                                console.log('Mise a jours des données...');
                                return new Promise(function (resolve, reject) {//Permet d'attendre la fin de la transaction pour effectuer la suite
                                    jQuery.ajax({
                                        method: "POST",
                                        url: "http://d-sound.fr/webservice/v1/getall",
                                        data: { 
                                            "id":userId
                                        }
                                    }).done(function( msg ){
                                        resolve(msg);                     
                                    });
                                }).then(function (msg) {
                                    var obj = jQuery.parseJSON( msg );
                                    console.log('AJAX INSERT PL', obj);

                                    //On créé une transaction pour exécuter la suite une fois que tous les items sont inséré en bdd (en mode ASYNC)
                                    return serv.db.transaction('rw', serv.db.playlists, serv.db.songs, serv.db.messages, serv.db.days, 
                                                            serv.db.dates, serv.db.diffusions, serv.db.plages, serv.db.messagesToPlage, 
                                                            serv.db.plannings, serv.db.daysToPlanning, function () {
                                    //On insère tous les items dans la BDD locale                        
                                    jQuery.each(obj.playlists, function(key, playlist){
                                        methods.insertPlaylist(playlist);
                                        methods.insertSongs(playlist);			
                                    });
                                    jQuery.each(obj.messages, function(key, message){  
                                        methods.insertMessage(message);
                                        methods.insertDaysToMessage(message);
                                        methods.insertDatesToMessage(message);	
                                    });
                                    jQuery.each(obj.plannings, function(key, planning){
                                        methods.insertPlanning(planning);
                                        methods.insertDaysInPlanning(planning);
                                        methods.insertDaysToPlanning(planning);
                                        methods.insertDatesToPlanning(planning);	
                                    });
                                    if(obj.diffusion){
                                       methods.insertDiffusion(obj.diffusion); 
                                    }

                                    //ws.send('{ "type":"text", "content":"Browser ready.", "id":"pc"}' ); 
                                    serv.db.open().catch(function(error) {
                                        console.log("Can't open the database! Error:" + error);
                                    });  
                                });

                            });
                            }else{
                                console.log('Donnees deja a jour');
                                return users;
                            }
                        }
                    });
                }
            });
                    
        };
        //insertion d'un utilisateur
        serv.insertUser = function(id, username, password){
            methods.insertUser(id, username, password);
        };
        
        serv.checkUser = async function(username, password){
            return serv.db.users.where ("[username+password]").equals([username, password]).toArray();
        };
        
        //recuperation et affichage des playlists
        serv.getPlaylists = async function(userId) {

            // Join all:
            return serv.joinPlaylists(serv.db.playlists.where('user_id').equals(parseInt(userId))).then(function (playlists) {
                return playlists;
            }).catch(function (error) {
                console.log("Oops: " + error);
            });

        };		
        //recuperation et affichage des playlists
        serv.resetCptTracks = function() {
            serv.cptdl = 0;
        };		
        //recuperation et affichage des messages
        serv.getMessages = async function(userId) {
            // Join all:
            var messagesCollection = (typeof userId != 'undefined')?serv.db.messages.where('user_id').equals(parseInt(userId)):serv.db.messages.toCollection();
            return serv.joinMessages(messagesCollection).then(function (messages) {
                return messages;
            }).catch(function (error) {
                console.log("Oops messages: " + error);
            });
        };	
        //recuperation et affichage des diffusion
        serv.getDiffusion = async function(userId) {
            // Join all:
            console.log('LAUNCH GET DIFFUSION');
            return serv.joinPlages(serv.db.diffusions.where('user_id').equals(parseInt(userId))).then(function (diffusions) {
                return diffusions;
            }).catch(function (error) {
                console.log("Oops: " + error);
            });
        };	
        //recuperation et affichage des diffusion 
        serv.getPlanning = async function(userId) {
            // Join all:
            console.log('LAUNCH GET PLANNING');
            return serv.joinPlannings(serv.db.plannings.where('user_id').equals(parseInt(userId))).then(function (plannings) {
                return plannings;
            }).catch(function (error) {
                console.log("Oops: " + error);
            });
        };	


        //server: Initialize
        methods.init();
    };


    //server: Default Settings
    $.server.defaults = {
        player: {} //Objet de gestion du player audio
    };
	
	$.fn.server = function(options) {
        if (options === undefined) { options = {}; }
        var $this = $(this);
        if (typeof options === "object") {
            if ($this.data('server') === undefined) {
                new $.server(this[0], options);
            }

        }
    };

})(jQuery);

//INSTANCE new $.server(options);
