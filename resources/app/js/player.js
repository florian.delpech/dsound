;

(function ($) {

    //serer: Object Instance : Communications avec le serveur electron
    $.player = function(el, options) {
		
        var play = $(el);
        // making variables public
        play.vars = $.extend({}, $.player.defaults, options);

        var methods = {};
		jQuery.data(el, "player", play);

        // Private serv methods
        methods = {
            init: function() {
                play.vars.tracks = [];
                play.vars.tracksBackup = [];
                play.vars.diffusionBackup = [];
                play.vars.playlistBackup = [];
                play.vars.randomPlay = true;
                play.vars.chronoPlay = false;
                play.vars.currentIndex = false;
                play.vars.currentPlaylist = false;
                play.vars.audioMessage;
                play.vars.mediaPath = 'uploads/songs/';
                play.vars.messagesPath = 'uploads/messages/';
                play.vars.callsPath = 'uploads/calls/';
                
                play.vars.weekday = new Array(7);
                play.vars.weekday[0]="dimanche";
                play.vars.weekday[1]="lundi";
                play.vars.weekday[2]="mardi";
                play.vars.weekday[3]="mercredi";
                play.vars.weekday[4]="jeudi";
                play.vars.weekday[5]="vendredi";
                play.vars.weekday[6]="samedi";

                play.loadTrack;
                play.init = false;			
                play.playerHtml;
                play.playTrack;
                play.tIdToAdd = false;
                play.timeout = false;
                play.vars.audio = false;
                play.shuffleBtn = play.find('.btnShuffle');
                play.interval = false;
                play.firstLecture = true;
                
                //Lance un titre / message après avoir vérifier qu'il existe via electron
                ipcRenderer.on('callback-check-file-exist', (event, url) => {
                    if(url){
                        play.vars.audio.src = url;
                        play.vars.audio.play().catch(function (error) {
                            if(!play.firstLecture){
                                ipcRenderer.send('delete-error-file', JSON.stringify(url));
                            }else{
                                play.firstLecture = false;
                            }                           
                            setTimeout(function(){
                                $('#btnNext').click();
                            }, 1000);
                            //$('#btnNext').click();
                        });
                        
                    }else{
                        $('#btnNext').click();
                    }                   
                });
                
                methods.firstInitPlaylist();//initialisation du lecteur js plyr
            },
            //Initialisation du playeur
            initPlayer: function(){
                				
                //BTN shuffle
                play.shuffleBtn.click(function(){
                   if(!play.vars.chronoPlay){
                           play.vars.chronoPlay = true; 
                           $(this).addClass('active'); 

                   }else{
                           play.vars.chronoPlay = false;
                           $(this).removeClass('active'); 
                   }
                });


                var supportsAudio = !!document.createElement('audio').canPlayType;
                if (supportsAudio) {

                        var track_number = 1;

                }
                
            },
            //Affiche le titre en cours de lecture en tête de liste
            hideTitles: function(){ 
                var $playlist = $('#plList');
                $('.li-display-none').removeClass('li-display-none');
                if(!$playlist.hasClass('open')){
                        $playlist.find('li').each(function(){
                                if($(this).hasClass('plSel')){
                                        return false;
                                }
                                $(this).addClass('li-display-none');
                        });
                }
            },
            //initialisation du lecteur js plyr
            firstInitPlaylist: function(){
                if(!play.init){
                   //initialize plyr
                    play.vars.plyr.setup($('#audio1'), {});
                    methods.initPlyr();
                    play.playerHtml = $('#audio1').get(0);
                    play.playerHtml.play(); 
                    play.init = true;
                }
            },
            //generation d un item de la playlist du lecteur
            generateHtmlItemPlaylist: function(value){
                var html = '';

                var html = '<li data-index="'+value.track+'" id="playing-track-'+value.id+'"><div class="plItem"><div class="plNum">';
                html += '<span>';
                var activePlId = $('#playing-playlist').attr('data-plid');

                if(typeof value.plage_id != 'undefined'){
                    html += '</span>&nbsp;</div><div class="plTitle">[Message] ' + value.name + '</div><div class="plLength"></div></div></li>';
                }else{
                    html += '</span>&nbsp;</div><div class="plTitle">' + value.title + '</div><div class="plLength">' + value.playtime_string + '</div></div></li>';
                }    
                return html;
            },
            //Initialisation global du lecteur
            initPlyr: function(playlist){
                if(typeof playlist === 'undefined'){
                        playlist = {"pid": 0};
                }
                methods.hideTitles();
                $( "#btnPrev").unbind( "click" );
                $( "#btnNext").unbind( "click" );
                $( "#plList li").unbind( "click" );
                $('#audio1').unbind( "play" );
                $('#audio1').unbind( "pause" );
                $('#audio1').unbind( "ended" );
                $('#plList').html('');
                
                play.vars.audio = $('#audio1').bind('play', function () {
                    playing = true;
                    npAction.text('Now Playing...');
                }).bind('pause', function () {
                    playing = false;
                    npAction.text('Paused...');
                }).bind('ended', function () {
                    npAction.text('Paused...');
                    if ((index + 1) < trackCount) {
                        index++;
                        loadTrack(index);
                        play.vars.audio.play();
                    } else {
                        //play.vars.audio.pause();
                        index = 0;
                        loadTrack(index);
                        play.vars.audio.play();
                    }
                }).get(0);
                
                var playing = false,
                //on parcours toutes les chanssons de la playlist a jouer et on genere l'html
                buildPlaylist = $.each(play.vars.tracks, function(key, value) {
                    var html = methods.generateHtmlItemPlaylist(value);	
                    $('#plList').append(html);
                }),
                trackCount = play.vars.tracks.length,
                index = (!play.vars.currentIndex || play.vars.tracks.length <= play.vars.currentIndex)?0:play.vars.currentIndex,
                npAction = $('#npAction'),
                npTitle = $('#npTitle'),				
                btnPrev = $('#btnPrev').click(function () {
                    if ((index - 1) > -1) {
                        index--;
                        loadTrack(index);
                        play.vars.audio.play();

                    } else {
                        //play.vars.audio.pause();
                        index = 0;
                        loadTrack(index);
                        play.vars.audio.play();                           
                    }
                }),
                btnNext = $('#btnNext').click(function () {
                    if ((index + 1) < trackCount && typeof play.vars.tracks[(index + 1)] != 'undefined') {
                        index++;
                        loadTrack(index);
                        play.vars.audio.play();
                    } else {
                        //play.vars.audio.pause();
                        index = 0;
                        loadTrack(index);
                        play.vars.audio.play();
                    }
                }),
                loadTrack = function (id) {
                    if(typeof play.vars.tracks[id] != 'undefined'){
                        $('.plSel').removeClass('plSel');
                        $('#plList li:eq(' + id + ')').addClass('plSel');
                        npTitle.text(play.vars.tracks[id].title);
                        index = id;
                        if(typeof play.vars.tracks[id].plage_id !== 'undefined'){
                            var inDatesToPlay = methods.checkCurrentDates(play.vars.tracks[id]);//On vérifie qu'il rentre bien dans les dates de diffusion
                            var inDaysToPlay = methods.checkCurrentDay(play.vars.tracks[id]);//On vérifie qu'il rentre bien dans les dates de diffusion
                            console.log('On verifie la date + jour du message', inDatesToPlay, inDaysToPlay);
                            if(!inDatesToPlay || !inDaysToPlay || play.vars.tracks[id].filename == ''){
                                    $('#btnNext').click();
                            }else{//On joue le message si date + jour est valide
                                //On lance la verification que le fichier existe bien. Le fichier est lancer dans "callback-check-file-exist"
                                console.log('check-message-exist', play.vars.messagesPath);
                                ipcRenderer.send('check-file-exist', JSON.stringify(play.vars.messagesPath + play.vars.tracks[id].filename));
                            }                                                    
                        }else{
                            //On lance la verification que le fichier existe bien. Le fichier est lancer dans "callback-check-file-exist"
                            console.log('check-file-exist', play.vars.mediaPath);
                            ipcRenderer.send('check-file-exist', JSON.stringify(play.vars.mediaPath + play.vars.tracks[id].filenamemp3));
                        }
                        //document.getElementById('contentframe').contentWindow.setAnimePlaying(play.vars.tracks[id].id);
                        play.vars.currentIndex = index;
                        methods.hideTitles();
                    }

                },
                li = $('#plList li').click(function () {
                    var id = parseInt($(this).index());
                    if (id !== index) {
                            play.playTrack(id);
                    }
                });
                //Initialisation des actions du lecteur de message instantanés
                $('#audio3').bind('play', function () {
                    document.getElementById("audio3").volume = 1;
                    play.vars.audio.pause();
                }).bind('ended', function () {
                    play.vars.audio.play();
                }).get(0);

                //lecture d'un message instantané (via btn)
                play.playAudioMessage = function(message){
                    var audioMessagePlayer = document.getElementById('audio3');
                    audioMessagePlayer.pause();
                    audioMessagePlayer.currentTime = 0;
                    audioMessagePlayer.src = play.vars.callsPath + message;
                    audioMessagePlayer.play();			
                };
                
                //lecture d'un message à heure fixe
                play.playAudioFixeMessage = function(message){
                    var audioMessagePlayer = document.getElementById('audio3');
                    audioMessagePlayer.pause();
                    audioMessagePlayer.currentTime = 0;
                    audioMessagePlayer.src = play.vars.messagesPath + message;
                    audioMessagePlayer.play();			
                };
                //lecture d'une musique
                play.playTrack = function (id) {
                    loadTrack(id);
                    play.vars.audio.play();
                };
                play.playTrack(index);

                //INIT OPTIONS
                $('.addsong').unbind('click');
                $('.addsong').click(function(){
                    event.stopPropagation();
                    addToPlaylist($(this));
                });

                //document.getElementById('contentframe').contentWindow.displayCurrentPlaying(play.vars.tracks);
                $('.open-list-pl').click(function(){
                        tIdToAdd = $(this).attr('data-tid'); 
                        $('.select-playlist-overlay').fadeIn(300);
                });
                $('.close-choice-pl').click(function(){
                        $('.select-playlist-overlay').fadeOut(300);
                });
            },
            //format une date dd/mm/YYYYY en YYYY-mm-dd
            formatDate: function(date, objDate){     
                if(objDate){
                    var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    return [year, month, day].join('-');
                }else{
                    var d = date.split('/');
                    return [d[2], d[1], d[0]].join('-');  
                }                       
            },
             //Verification de la date courante 
            checkCurrentDates: function(message){
                var state = false;
                if(message.active_year == '1'){
                    return true;
                }
                $.each(message.dates, function(key, date) {
                    var start = methods.formatDate(date.start_date);
                    var stop = methods.formatDate(date.end_date);

                    var d = methods.formatDate(new Date(), true);
                    if(Date.parse(d) >= Date.parse(start) 
                            && Date.parse(d) <= Date.parse(stop))
                    {
                        state = true;
                    }                 
                });
                
                return state;
            },
            isGoodDay: function(days){
                var date = new Date();
                var currentDay = date.getUTCDay();
                var isGoodDay = false;
                $.each(days, function(key, day) {
                    if(day.day_order == currentDay){
                        isGoodDay = true;
                    }              
                });
                
                return isGoodDay;
            },
            checkCurrentDay: function(message){
                var date = new Date();
                var currentDay = date.getUTCDay();
                //console.log('CHECK CURRENT DAY', message, message.active_week, methods.isGoodDay(message.jours), currentDay);
                if(message.active_week == '1' || methods.isGoodDay(message.jours)){
                    return true;
                }else{
                    return false
                }
            },
            playPlaylist: function(playlistToPlayId){
                var $playlist = $('#select-playlist-'+playlistToPlayId);
                if($playlist.length > 0){
                    $playlist.click();
                }else{
                    setTimeout(function(){
                        methods.playPlaylist(playlistToPlayId);
                    }, 500);
                }
            },
            lunchPlanning: function(plannings, messages){
                //on check le jour courant
                var date = new Date();

                //@TODO ajouter securité pour éviter boucle infinie
                var currentDay = play.vars.weekday[date.getUTCDay()];
                if(plannings.length > 0 && plannings[0].plannings[currentDay].length > 0){
                    //@TODO Selectionner le planning le plus prioritaire
                    var playlistToPlayId = false;
                    var date = new Date;
                    var minutes = date.getMinutes();
                    var hours = date.getHours();
                    hours = ("0" + hours).slice(-2);

                    console.log('ON REGARDE SI ON DOIT LANCER UNE PLAYLIST OU UN MESSAGE');    
                    //On parcours les plannins pour savoir lequel lancer
                    $.each(plannings[0].plannings[currentDay], function(key, day) {
                        var start = day.start.split(':');
                        var startHours = start[0];             
                        var startMinutes = start[1];
                        var end = day.end.split(':');
                        var endHours = end[0];             
                        var endMinutes = end[1];

                        if( (parseInt(startHours) < parseInt(hours) ||  
                            (parseInt(startHours) == parseInt(hours) && parseInt(startMinutes) <= parseInt(minutes))) 
                            && 
                            (parseInt(endHours) > parseInt(hours) || 
                            parseInt(endHours) == parseInt(hours) && parseInt(endMinutes) > parseInt(minutes)) )
                        {
                            playlistToPlayId = day.playlist_id;
                        }
                    });
                    //On parcours les messages à jouer
                    $.each(messages, function(key, message) {
                        if(message.type_play == 'fixe'){
                            var inDatesToPlay = methods.checkCurrentDates(message);//On vérifie qu'il rentre bien dans les dates de diffusion
                            var inDaysToPlay = methods.checkCurrentDay(message);//On vérifie qu'il rentre bien dans les dates de diffusion
                            //console.log('INSERT MESSAGE INTO PLAYLIST', inDaysToPlay, inDatesToPlay, message);
                            if(inDatesToPlay && inDaysToPlay){
                                var start = message.hour_send.split(':');
                                var startHours = start[0];             
                                var startMinutes = start[1];
                                if( (parseInt(startHours) == parseInt(hours) &&  parseInt(startMinutes) == parseInt(minutes)) )
                                {
                                    console.log('MESSAGE A JOUER', message);
                                    play.playAudioFixeMessage(message.filename);
                                }
                            }
                        }
                        
                    });
                    
                    if(play.vars.currentPlaylist !== playlistToPlayId){
                        play.vars.currentPlaylist = playlistToPlayId;
                        methods.playPlaylist(playlistToPlayId);
                    }
                    console.log('PLANNING TO PLAY', playlistToPlayId);
                }
            }
			    
        };

        // public methods
        play.timerShedulePlay = function(plannings, messages){
            methods.lunchPlanning(plannings, messages);//On lance la playlist a jouer
                        
            //On programme la vérification de la playlist a jouer toutes les minutes a 00 secondes
            var d = new Date();
            var n = 60000 - d.getSeconds()*1000;
            console.log('On attend '+n+' secondes avant de vérifier la playlist a jouer');
            setTimeout(function(){
                console.log('Timeout ok! On lance le set interval');
                if(play.interval) clearInterval(play.interval);
                play.interval = setInterval(function() {
                    console.log('Setinterval ok! On lance le verifie la playlist');
                    methods.lunchPlanning(plannings, messages);
                }, 60 * 1000);                        
            }, n);
        };
		
        play.shuffle = function(array) {
                var currentIndexShuffle = array.length, temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndexShuffle) {

                  // Pick a remaining element...
                  randomIndex = Math.floor(Math.random() * currentIndexShuffle);
                  currentIndexShuffle -= 1;

                  // And swap it with the current element.
                  temporaryValue = array[currentIndexShuffle];
                  array[currentIndexShuffle] = array[randomIndex];
                  array[randomIndex] = temporaryValue;
                }

                return array;
        };

        //Ajout de titres à la liste de lecture (remplace les anciens titres)
        play.addTracks = function(trks, playlist, diffusions){
            //Permet de save les trks, playlist et diffusions afin de les réutiliser en cas de réinitialisation
            if(typeof trks === 'undefined'){
                trks = play.vars.tracksBackup;
            }else{
                play.vars.tracksBackup = trks.slice();
            }
            if(typeof playlist === 'undefined'){
                playlist = play.vars.playlistBackup;
            }else{
                play.vars.playlistBackup = playlist;
            }
            if(typeof diffusions === 'undefined'){
                diffusions = play.vars.diffusionBackup;
            }else{
                play.vars.tracksBackup = diffusions.slice();
            }
            $('#current-playlist .playlist-container').html(''); 
            $('#playing-playlist').html('En cours : '+playlist.name).attr('data-plid', playlist.id);
            play.vars.tracks = [];//On vide la playlist en cours
            
            //On calcule le nombre de musiques / message d'un cycle de diffusion
            var tracksCycleDiffusionNb = 0;
            var nbTracksTotal = 1500;
            
            if(diffusions.length > 0){
                $.each(diffusions[0].plages, function(key, plage) {
                    tracksCycleDiffusionNb += parseInt(plage.nbSongs);
                }); 
            }
            

            //on dupplique la playlist pour arriver a 1500 titres
            var cpt = Math.ceil(nbTracksTotal/trks.length)-1;
            var cloneTrks = trks.slice(0);
            for(var i =0; i< cpt; i++){
                trks = trks.concat(play.shuffle(cloneTrks));
            }
           
            
            $.each(trks, function(key, value) {
                value.pl = playlist.id;
                play.vars.tracks.unshift(value); 
            });
            $.each(play.vars.tracks, function(key, value) {
                value.track = (key+1);
            });
            
            var indexInsertTmp = 0;
          
            //On insert les message dans la playlist de 3000 titres
            if(diffusions.length > 0){
                var nbBoucle = nbTracksTotal / tracksCycleDiffusionNb;
                for(var i =0; i< nbBoucle; i++){
                    $.each(diffusions[0].plages, function(key, plage) {
                        var indexToInsert = indexInsertTmp + parseInt(plage.nbSongs);
                        $.each(plage.messages, function(key, message) {
                            if(indexToInsert < play.vars.tracks.length){
                                var inDatesToPlay = methods.checkCurrentDates(message);//On vérifie qu'il rentre bien dans les dates de diffusion
                                var inDaysToPlay = methods.checkCurrentDay(message);//On vérifie qu'il rentre bien dans les dates de diffusion
                                //console.log('INSERT MESSAGE INTO PLAYLIST', inDaysToPlay, inDatesToPlay, message);
                                if(inDatesToPlay && inDaysToPlay){
                                    play.vars.tracks.splice(indexToInsert, 0, message);
                                    indexToInsert++;
                                }
                            }else{
                                return false;
                            }                       
                        });
                        indexInsertTmp = parseInt(indexToInsert);

                    });
                }
            }
            console.log('PLAYLIST FULL => play.vars.tracks', play.vars.tracks);
            
            methods.initPlyr(playlist);
        };
        //Ajout d'un titre a la liste de lecture
        play.addTrack = function(track){

            if(typeof track.title != 'undefined' && typeof track.file != 'undefined' && typeof track.length != 'undefined'){
                track.pl = 0;
                play.vars.tracks.unshift(track);  
            }
            $.each(play.vars.tracks, function(key, value) {
                    value.track = (key+1);
            });
            methods.initPlyr();
            play.playTrack(0);
        };
        //Lecture d'un message
        play.playMessage = function(message){
            console.log('Play message', message);
            npTitle.text(message.title);
            play.vars.audio.src = play.vars.messagesPath + message.filenamemp3;

            play.vars.audio.play();
        };

        //server: Initialize
        methods.init();
    };

    
    $.fn.player = function(options) {
        if (options === undefined) { options = {}; }
        var $this = $(this);
        if (typeof options === "object") {

            if ($this.data('player') === undefined) {
				//server: Default Settings
                new $.player(this[0], options);
            }

        }
    };

})(jQuery);
