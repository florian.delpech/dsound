;

(function ($) {

    //serer: Object Instance : Communications avec le serveur electron
    $.playlists = function(el, options) {
		
        var list = $(el);
        // making variables public
        list.vars = $.extend({}, $.playlists.defaults, options);

        var methods = {};
		jQuery.data(el, "playlists", list);

        // Private serv methods
        methods = {
            init: function() {
                list.vars.tracks = [];		
                list.vars.playing = false;
                methods.firstInitPlaylists();
            },
			//Initialisation
            firstInitPlaylists: function(){               				
								
            }
			    
        };

        // public methods
		
        list.displayPlaylists = function(playlists, diffusions, $player) {

            var $container = $('.list-playlist-container');
            $container.html('');
            jQuery.each(playlists, function(index, playlist) {

                var playlistHtml = '<div id="select-playlist-'+playlist.id+'" class="select-playlist" >';
                playlistHtml += '<div data-lgt="6" data-id="'+playlist.id+'" data-display="edit-playlist" class="go-select-playlist"><div class="picture">';
                var i = 0;
                var cpt = 0;

                jQuery.each(playlist.songs, function(index, item) {
                    
                    if(i<=3 && item.picturelocal !== ''){
                        if(typeof item.picturelocal !== 'undefined' && item.picturelocal !== '' && item.picturelocal !== null){
                            playlistHtml += '<img width="100" height="100" src="'+item.picturelocal+'" />';
                            i++;
                            cpt++;
                        }
                    }
                });
                playlistHtml += '</div>';
                playlistHtml += '<div class="playlist-name">'+playlist.name+'</div>';
                playlistHtml += '<div class="playlist-sub-name">'+playlist.songscount+' titres</div>';
                playlistHtml += '</div>';
                playlistHtml += '</div>';

                $container.append(playlistHtml);

                //jouer la playlist au click
                $('#select-playlist-'+playlist.id).unbind('click');
                $('#select-playlist-'+playlist.id).click(function(){
                    $player.addTracks(playlist.songs, playlist, diffusions);
                });

                //On lance la premiere playlist
                //@TODO lancer la plus prioritaire 
                if(index == 0 && !list.vars.playing){
                    list.vars.playing = true;
                    $('#select-playlist-'+playlist.id).click();
                }
                
            });
                //initSearchInPlaylist();
                //initialisationEditPlaylist();
        };

		

        //server: Initialize
        methods.init();
    };
    
    $.fn.playlists = function(options) {
        if (options === undefined) { options = {}; }
        var $this = $(this);
        if (typeof options === "object") {

            if ($this.data('playlists') === undefined) {
				//server: Default Settings
                new $.playlists(this[0], options);
            }

        }
    };

})(jQuery);
